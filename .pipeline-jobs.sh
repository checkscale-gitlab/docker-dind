#!/usr/bin/env bash
set -Eeuo pipefail

[ -f versions.json ] # run "versions.sh" first

if [ "$#" -eq 0 ]; then
	flavors="$(jq -r 'keys | map(@sh) | join(" ")' versions.json)"
	eval "set -- $flavors"
fi

cat >.pipeline-jobs.yml <<-EOH
#
# NOTE: THIS DOCKERFILE IS GENERATED VIA "$0"
#
# PLEASE DO NOT EDIT IT DIRECTLY.
#
## -------------------------------------------------- ##
EOH

for flavor; do
	export flavor
	suite=${flavor%/*}
	arch=${flavor#*/}

	cat >>.pipeline-jobs.yml <<-EOH
	
	child:${suite}:${arch}:
	  extends: .child-pipeline
	  variables:
	    PIPELINE_IMAGE_PREFIX: \${CI_REGISTRY_IMAGE}/staging:\${CI_PIPELINE_ID}-
	    PIPELINE_SERIES: "${suite}"
	    PIPELINE_ARCH: "${arch}"
	EOH

	issues="$(jq -r '.[env.flavor] | select(.issues != null) | .issues | map(@sh) | join(" ")' versions.json)"
	{
		for issue in ${issues}; do
			issue="${issue#\'}"
			issue="${issue%\'}"
			echo "  # ${issue}"
		done
		[ -z "${issues}" ] || echo "  allow_failure: true"
	} >>.pipeline-jobs.yml

	cat >>.pipeline-jobs.yml <<-EOH
	
	## -------------------------------------------------- ##
	EOH
done
