#
# NOTE: THIS DOCKERFILE IS GENERATED VIA "apply-templates.sh"
#
# PLEASE DO NOT EDIT IT DIRECTLY.
#

ARG BASE_IMAGE=vicamo/ubuntu:xenial-armhf

FROM ${BASE_IMAGE}

RUN apt-get update --quiet \
	&& DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes \
		ca-certificates \
# DOCKER_HOST=ssh://... -- https://github.com/docker/cli/pull/1014
		openssh-client \
		wget \
	&& (ls /etc/ssl/certs/*.0 || c_rehash) \
	&& rm -rf /var/lib/apt

ENV DOCKER_VERSION 20.10.2
# TODO ENV DOCKER_SHA256
# https://github.com/docker/docker-ce/blob/5b073ee2cf564edee5adca05eee574142f7627bb/components/packaging/static/hash_files !!
# (no SHA file artifacts on download.docker.com yet as of 2017-06-07 though)

RUN set -eux; \
	\
	dpkgArch="$(dpkg --print-architecture)"; \
	case "$dpkgArch" in \
		'amd64') \
			url='https://download.docker.com/linux/static/stable/x86_64/docker-20.10.2.tgz'; \
			;; \
		'armel') \
			url='https://download.docker.com/linux/static/stable/armel/docker-20.10.2.tgz'; \
			;; \
		'armhf') \
			url='https://download.docker.com/linux/static/stable/armhf/docker-20.10.2.tgz'; \
			;; \
		'arm64') \
			url='https://download.docker.com/linux/static/stable/aarch64/docker-20.10.2.tgz'; \
			;; \
		*) echo >&2 "error: unsupported architecture ($dpkgArch)"; exit 1 ;; \
	esac; \
	\
	wget --quiet -O docker.tgz "$url"; \
	\
	tar --extract \
		--file docker.tgz \
		--strip-components 1 \
		--directory /usr/local/bin/ \
	; \
	rm docker.tgz; \
	\
	dockerd --version; \
	docker --version

COPY modprobe.sh /usr/local/bin/modprobe
COPY docker-entrypoint.sh /usr/local/bin/

# https://github.com/docker-library/docker/pull/166
#   dockerd-entrypoint.sh uses DOCKER_TLS_CERTDIR for auto-generating TLS certificates
#   docker-entrypoint.sh uses DOCKER_TLS_CERTDIR for auto-setting DOCKER_TLS_VERIFY and DOCKER_CERT_PATH
# (For this to work, at least the "client" subdirectory of this path needs to be shared between the client and server containers via a volume, "docker cp", or other means of data sharing.)
ENV DOCKER_TLS_CERTDIR=/certs
# also, ensure the directory pre-exists and has wide enough permissions for "dockerd-entrypoint.sh" to create subdirectories, even when run in "rootless" mode
RUN mkdir /certs /certs/client && chmod 1777 /certs /certs/client
# (doing both /certs and /certs/client so that if Docker does a "copy-up" into a volume defined on /certs/client, it will "do the right thing" by default in a way that still works for rootless users)

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["sh"]
