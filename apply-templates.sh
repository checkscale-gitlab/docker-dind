#!/usr/bin/env bash
set -Eeuo pipefail

[ -f versions.json ] # run "versions.sh" first

jqt='.jq-template.awk'
if [ -n "${BASHBREW_SCRIPTS:-}" ]; then
	jqt="$BASHBREW_SCRIPTS/jq-template.awk"
elif [ "$BASH_SOURCE" -nt "$jqt" ]; then
	wget -qO "$jqt" 'https://github.com/docker-library/bashbrew/raw/5f0c26381fb7cc78b2d217d58007800bdcfbcfa1/scripts/jq-template.awk'
fi

if [ "$#" -eq 0 ]; then
	flavors="$(jq -r 'keys | map(@sh) | join(" ")' versions.json)"
	eval "set -- $flavors"
fi

generated_warning() {
	cat <<-EOH
		#
		# NOTE: THIS DOCKERFILE IS GENERATED VIA "apply-templates.sh"
		#
		# PLEASE DO NOT EDIT IT DIRECTLY.
		#

	EOH
}

for flavor; do
	export flavor
	# To be complatible with downloaded jq-template.awk
	export version=$flavor

	variants="$(jq -r '.[env.flavor].variants | map(@sh) | join(" ")' versions.json)"
	eval "variants=( $variants )"

	for variant in "${variants[@]}"; do
		template="Dockerfile${variant:+-$variant}.template"
		target="$flavor${variant:+/$variant}/Dockerfile"

		{
			generated_warning
			gawk -f "$jqt" "$template"
		} > "$target"
	done

	cp -a docker-entrypoint.sh modprobe.sh "$flavor/"
	cp -a dockerd-entrypoint.sh "$flavor/dind/"
done
